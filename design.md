##Game

- Not tile based
- Bleed health
- Arena after arena with rest stops
- Randomized mutations for end game

##System

- Actor contains basic variables and functionality
	- Derived enemy/hero contains spells and attribute for actor to use
	- Bleed is a second health bar
		- Bleed takes full damage, health takes percent inversely on defense
		- Healing sets bleed = health
		- every turn health-- if bleed is lower
	- Players contain inventory `std::array<*Item>` and a skill tree
- Avoid up or down casting team pointers
- Town area to buy items and place bets
- Items contain stat bonuses and a spell, which can be used, on-hit, or on-cast
- Do mythic plus

