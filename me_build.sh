pwd

FORCED="false"
for opt in $@; do
	if [ "$opt" == "-f" ] || [ "$opt" == "--force" ]; then
		FORCED="true"
	elif [ -f $opt ]; then
		SOURCES=${opt}
	else
		BUILD_DIR=${opt}
	fi
done

# uses first input file to build, or if none supplied the entire src directory
if [ -z "${SOURCES}" ]; then
	SOURCES=$(find src/ oven/ -regex '.*\(\(.cpp\)\|\(.c\)\)') || exit
fi

# compilers
CXX="g++"
C="gcc"

# not really needed
if [ -z "${BUILD_DIR}" ]; then
	BUILD_DIR="obj"
fi

# create or use build specified directory
if [ -d ${BUILD_DIR} ]; then
	echo "GERT: Using build dir ${BUILD_DIR}"
else
	echo "GERT: Creating build dir ${BUILD_DIR}"
# gets directory structure of source and oven, and mimics it inside BUILD_DIR
	SRC_ACTIVE_PATH=$(find src/ oven/ -type d)
	for p in ${SRC_ACTIVE_PATH}; do
		mkdir -p ${BUILD_DIR}/${p}
		echo "${BUILD_DIR}/${p}"
	done
fi

# check if sources is a non-zero string
if [ -z "${SOURCES}" ]; then
	echo "GERT: Error, no sources."
	exit
else
	echo "GERT: Building...."
fi

# build sources individually
# -nt checks if first argument was edited more recently than the latter
# or true if the latter does not exist
for src in ${SOURCES}; do
	if [ $FORCED == "true" ] || [ ${src} -nt ${BUILD_DIR}/${src}.o ]; then
		if [ ${src: -4} == ".cpp" ]; then
			echo -e "\e[36mBuilding CXX: ${src}\e[0m"
			${CXX} -c ${src} -o ${BUILD_DIR}/${src}.o -I"include" -Wall
		else
			echo -e "\e[94mBuilding C: ${src}\e[0m"
			${C} -c ${src} -o ${BUILD_DIR}/${src}.o
		fi
	# else
		# echo -e "\e[36mDoesn't need building ${src}\e[0m"
	fi
done

echo "GERT: Linking...."
OBJECTS=$(find ${BUILD_DIR} -iname '*.o')
${CXX} ${OBJECTS} -o goat_tested_colosseum -lSDL2main -lSDL2 -lGLEW -lGL
