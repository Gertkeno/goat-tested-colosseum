# Game Data Structure

## Blocking

Entities are declared in curly brace `{}` blocks, this allows you to define tags in any order you prefer. This allows for warning when creating entities with missing data.

## Tags

Most entity data is defined by tags, structured as.

`tag: data`

Specific cases tags allow more data and require a comma to end.

`tag: one, two, three,`

If you only need one data you need to end these with a comma. These tags will be marked in this document with `[]`

## Comments

Any line beginning with `#` is ignored.

    tag1: a
     #ignore me :)
    tag2: b

# Spells

## title:

Sets the name of the spell, this is later used when creating enemies and skill trees.

## description:

Sets a description of the spell, used in game.

## target: []

What the spell can cast on. Mix the following

    ground
    self
    ally
    enemy
    any

## range:

The farthest the spell can be cast from.

## ap:

Action points required to cast this spell.

## cd:

Turns until the spell is ready to use after the last use.

## Byte Code

Spells contain a special byte code, to start a byte code block use curly braces `{}`

    title: bad teleport
    description: teleports the caster to position (4, 2)
    {
      LITERAL 2
      LITERAL 4
      CASTER SET_POS_ABS
    }

The following will outline the Byte code words. Byte code words only require space separation.

- LITERAL #
    - Pushes the number to the stack.
- TARGET
    - Pushes the number 0 to the stack.
    - Other words read 0 to select the spell's target instead of caster.
- CASTER
    - Pushes the number 1 to the stack.
    - Other words read 1 to select the spell's caster instead of target.
- PUSH_POS
    - Pushes either the target or caster's position to the stack, y then x.
- PUSH_CURSOR
    - Pushes the cursor's position to the stack, y then x.
- ATTRIBUTE #
    - Pushes either the target or caster's attribute to the stack.
    - # must be one of the following
        - POISE
        - SPEED
        - STRENGTH
        - MAGIC
        - DEFENSE
- SET\_POS\_REL
    - Moves either the target or caster.
- SET\_POS\_ABS
    - Sets the position of either the target or caster, pops x then y.
- DAMAGE
    - Damages either target or caster, damage is last pushed value.
- ADD
    - Adds the last two values and pushes the result.
- SUB
    - Subtracts x - y, pushes the result.
- MUL
    - Multiplies two values, pushes the result.
- DIV
    - Divides x / y, pushes the result.
- SUB_VEC
    - Subtracts vecx - vecy, pushes the result.
- NORMAL_VEC
    - Normalizes the last two values as a vector.
- NEGATIVE
    - Pushes the last value as a negative.

# Enemies

## title:

Name the creature.

## Setting Base Attributes

- poise
- speed
- strength
- magic
- defense

Replace `attribute` with any of the above.

    attribute: #

Unset attributes will default to zero.

## radius:

How large the creature's hurt circle is.

## texture:

File name of creatures sprite.

## spells: []

Sets the spells this creature has. Specify which spell subset to search with `@subset,` spells listed after this will be searched for in said subset.

    spells: heal, @foo, root,

This example `heal` will be from the base subset and `root` will be from a subset called `foo`.
