#version 330
#extension GL_ARB_explicit_uniform_location : enable
in vec2 TexCoord;

layout (location=4) uniform vec4 uvslice;
layout (location=6) uniform vec3 colorMod;
layout (location=20) uniform sampler2D tex0;

layout (location=0) out vec3 outputDraw;

void main()
{
	vec2 ct_tex = TexCoord * uvslice.zw + uvslice.xy;
	vec3 color = texture2D (tex0, ct_tex).rgb;
	if (color == vec3(1.0, 0.0, 1.0))
		discard;

	outputDraw = color * colorMod;// * vec3(TexCoord,1.0);
}
