#pragma once
#include "Actor.hpp"

class Spell;

class Enemy: public Actor
{
public:
	using spell_c = std::array <const Spell *, ACTOR_SPELL_COUNT>;

	Enemy (const AttributeSet &, float r, gert::Texture *, const spell_c &, float l);
	Enemy (const Enemy &);

	float get_level() const {return _level;}

	const Spell * get_spell (unsigned i) const override;
private:
	const spell_c _spells;
	const float _level;
};
