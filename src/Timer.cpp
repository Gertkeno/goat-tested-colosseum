#include "Timer.hpp"
#include "FrameTime.hpp"

using namespace gert;

Timer::Timer( double max_seconds )
: _max( max_seconds )
, _start(0)
{}

void Timer::reset()
{
	_start = 0;
}

double Timer::since()
{
	return _start;
}

bool Timer::inc_check()
{
	_start += FrameTime::get_pure();
	return _start >= _max;
}

void Timer::mutate_max( double m )
{
	_max = m;
}
