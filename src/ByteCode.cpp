#include "ByteCode.hpp"

#include "Actor.hpp"
#include "DebugOut.hpp"

#include <stdexcept>
#include <cstring>

#ifndef NDEBUG
#define ADD_CONTEXT(x) strContext += x
#else
#define ADD_CONTEXT(x) ;
#endif

namespace bc
{

namespace
{
	std::vector <float> _stack;

	float pop()
	{
		if (_stack.empty())
			throw std::runtime_error {"Popped empty stack"};
		const auto foo {_stack.back()};
		_stack.pop_back();
		return foo;
	}

	void push (float s)
	{
		_stack.push_back (s);
	}
}

void run (const bc::key * c, Actor * target, Actor * caster, glm::vec2 point)
{
	const auto ctGetter {[&] (float v)
	{
		if (v == 0)
			return target;
		return caster;
	}};

#ifndef NDEBUG
	std::string strContext {"  ["};
#endif
	using namespace bc;
	while (c->word != END)
	{
		try
		{
		switch (c->word)
		{
		case PL:{
			const auto val {*++c};
			push (val.value);
			ADD_CONTEXT ("Push " + std::to_string (val.value));
			}break;
		case TARGET:
			push (0.0f);
			ADD_CONTEXT ("Target");
			break;
		case CASTER:
			push (1.0f);
			ADD_CONTEXT ("Caster");
			break;
		case SET_POS_REL:{
			const auto victim {ctGetter (pop())};
			const auto x {pop()};
			const auto y {pop()};
			victim->set_pos_relative ({x, y});
			ADD_CONTEXT ("SetRelPos");
			}break;
		case SET_POS_ABS:{
			const auto victim {ctGetter (pop())};
			const auto x {pop()};
			const auto y {pop()};
			victim->set_pos_absolute ({x, y});
			ADD_CONTEXT ("SetAbsPos");
			}break;
		case DAMAGE:{
			const auto victim {ctGetter (pop())};
			victim->take_damage (pop());
			ADD_CONTEXT ("Damage");
			}break;
		case PUSH_POS:{
			const auto victim {ctGetter (pop())};
			const auto p {victim->get_position()};
			push (p.y);
			push (p.x);
			ADD_CONTEXT ("PushPos");
			}break;
		case PUSH_ATTRIB:{
			const int index (pop());
			const auto victim {ctGetter (pop())};
			push (victim->get_attrib (static_cast <attrib_t> (index)));
			ADD_CONTEXT ("PushAttrib " + std::to_string (index));
			}break;
		case PUSH_CURSOR:
			push (point.y);
			push (point.x);
			ADD_CONTEXT ("PushCursor");
			break;
		case ADD:
			push (pop() + pop());
			ADD_CONTEXT ("Add");
			break;
		case SUB:
			push (pop() - pop());
			ADD_CONTEXT ("Subtract");
			break;
		case DIVIDE:
			push (pop() / pop());
			ADD_CONTEXT ("Divide");
			break;
		case MULTIPLY:
			push (pop() * pop());
			ADD_CONTEXT ("Multiply");
			break;
		case SUB_VEC:{
			const auto x1 {pop()};
			const auto y1 {pop()};
			const auto x2 {pop()};
			const auto y2 {pop()};
			push (y1 - y2);
			push (x1 - x2);
			ADD_CONTEXT ("SubVec");
			}break;
		case NORMAL_VEC:{
			const auto x {pop()};
			const auto y {pop()};
			const auto n {glm::normalize (glm::vec2 {x, y})};
			push (n.y);
			push (n.x);
			ADD_CONTEXT ("NormalVec");
			}break;
		case NEGATIVE:
			push (-pop());
			ADD_CONTEXT ("Negative");
			break;
		case END:
			break;
		default:
			throw std::runtime_error {"Unkown bytecode: " + std::to_string (c->word)};
		}
		ADD_CONTEXT (", ");
		++c;
		} // try
		catch (const std::exception & e)
		{
#ifndef NDEBUG
			ADD_CONTEXT ("...]");
			throw std::runtime_error {e.what() + strContext};
#else
			throw e;
#endif
		}
	}

#ifndef NDEBUG
	if (not _stack.empty())
	{
		DE_OUT ("Warning, bytecode finished but the stack still has data! (" << _stack.size() << ')');
		_stack.clear();
	}
#else
	_stack.clear();
#endif
}

bytecode_t str_to_code (const char * str)
{
	struct
	{
		const char * name;
		bytecode_t type;
	} static constexpr values[]
	{
		{"TARGET", TARGET},
		{"CASTER", CASTER},
		{"PUSH_POS", PUSH_POS},
		{"PUSH_CURSOR", PUSH_CURSOR},
		{"SET_POS_REL", SET_POS_REL},
		{"SET_POS_ABS", SET_POS_ABS},
		{"DAMAGE", DAMAGE},
		{"ADD", ADD},
		{"SUB", SUB},
		{"DIV", DIVIDE},
		{"MUL", MULTIPLY},
		{"SUB_VEC", SUB_VEC},
		{"NORMAL_VEC", NORMAL_VEC},
		{"NEGATIVE", NEGATIVE},
	};

	for (auto & i : values)
		if (std::strcmp (i.name, str) == 0)
			return i.type;
	throw std::runtime_error {"Couldn't find command for str " + std::string (str)};
}
} // bc
