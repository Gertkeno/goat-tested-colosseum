#include "Draw.hpp"

#include <GL/glew.h>
#include "Texture.hpp"
#include <glm/gtc/type_ptr.hpp>

namespace
{
	constexpr float square_verts[]{
		-1.0f, -1.0f, 0.0f, 0.0f, 1.0f, //bl
		-1.0f,  1.0f, 0.0f, 0.0f, 0.0f, //tl
		 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, //br
		 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, //tr
	};

	constexpr auto vert_width {5};
	constexpr auto verts_total {sizeof(square_verts)/sizeof(*square_verts)/vert_width};

	unsigned _arrayObject, _vertexArray;
}

void draw_init()
{
	glGenVertexArrays (1, &_arrayObject);
	glGenBuffers (1, &_vertexArray);

	glBindVertexArray (_arrayObject);
	glBindBuffer (GL_ARRAY_BUFFER, _vertexArray);
	glBufferData (GL_ARRAY_BUFFER, sizeof (square_verts), square_verts, GL_STATIC_DRAW);

	//position vertices
	glEnableVertexAttribArray (0);
	const auto stride {sizeof (float)*vert_width};
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, stride, nullptr);
	//texture UV
	glEnableVertexAttribArray (1);
	glVertexAttribPointer (1, 2, GL_FLOAT, GL_FALSE, stride, (void*)(3*sizeof(float)));

	std::atexit ([]
	{
		glDeleteVertexArrays (1, &_arrayObject);
		glDeleteBuffers (1, &_vertexArray);
	});
}

void Draw (const glm::mat4& pos, const gert::Texture * tex, glm::vec4 slice, glm::vec3 color)
{
	if (tex != nullptr)
		tex->set_active();

	glUniform4fv (4, 1, glm::value_ptr (slice));
	glUniformMatrix4fv (5, 1, GL_FALSE, glm::value_ptr (pos));

	glUniform3fv (6, 1, glm::value_ptr (color));
	glDrawArrays (GL_TRIANGLE_STRIP, 0, verts_total);
}
