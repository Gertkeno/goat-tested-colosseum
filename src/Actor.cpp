#include "Actor.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include "SpellCollector.hpp"
#include "GameExcept.hpp"
#include <SDL2/SDL_timer.h>

#include <iostream>

Actor::Actor (const AttributeSet& as, float r, gert::Texture * t)
: _baseAttribs {as}
, _radius {r}
, _texture {t}
, _cooldowns {0}
, _lastCast {0}
, actionPoints {0.0f}
{
	_node.myTexture = _texture;
}

Actor::Actor (const Actor & as)
: _baseAttribs {as._baseAttribs}
, _radius {as._radius}
, _texture {as._texture}
, _lastCast {0}
, actionPoints {0.0f}
{
}

bool Actor::get_collision (Sphere b) const
{
	const auto dist {glm::distance (b.pos, get_position())};
	return dist < (b.radius + _radius);
}

constexpr float aclamp (const float & in, const float & l, const float & r)
{
	auto low {std::min (l, r)};
	auto high {std::max (l, r)};
	if (in < low)
		return low;
	if (in > high)
		return high;
	return in;
}

inline glm::vec3 pill_clamp (const glm::vec3 & pos, const Pill & c)
{
	const auto x {aclamp (pos.x, c.a.x, c.b.x)};
	const auto y {aclamp (pos.y, c.a.y, c.b.y)};
	const auto z {aclamp (pos.z, c.a.z, c.b.z)};
	return {x, y, z};
}

bool Actor::get_collision (Pill p) const
{
	// https://forum.unity.com/threads/how-do-i-find-the-closest-point-on-a-line.340058/
	// might be wrong, testing will be annoying
	// tested, pretty accurate, will need to tune max/min spheres
	const auto dir {glm::normalize (p.b - p.a)};
	const auto dist {glm::dot (_position - p.a, dir)};
	const Sphere close {pill_clamp (p.a + dir * dist, p), p.radius};

	return get_collision (close);
}

void Actor::_update_mat()
{
	_node.edit_mat (glm::translate (glm::mat4(), _position));
}

void Actor::take_damage (float dmg)
{
	if (dmg < 0.0)
	{
		_health -= dmg;
		_bleed = _health;
		return;
	}
	_bleed -= dmg;
	_health -= dmg * get_reduction();
}

void Actor::cast_spell (unsigned i, Actor * t, const glm::vec2 & c, int turnCount)
{
	const auto s {get_spell (i)};
	if (turnCount - _cooldowns [i] < s->cooldown)
		throw ActorError {this, "Not cool enough B)"};
	if (actionPoints < s->actionPoints)
		throw ActorError {this, "Not enough Action Points"};
	if (t != nullptr)
	{
		const auto dist {glm::distance (get_position(), t->get_position())};
		if (dist > s->range)
			throw ActorError {t, "Target too far"};
	}

	_lastCast = i;
	_cooldowns [i] = turnCount;
	actionPoints -= s->actionPoints;
	bc::run (s->code, t, this, c);
}

void Actor::basic_attack (Actor * target)
{
	target->take_damage (get_attrib (STRENGTH) + get_attrib (POISE)/3);
}

void Actor::turn_update()
{
	if (_health > _bleed)
		_health = std::max (_health - 10, _bleed);
}
