#pragma once

#include "Node.hpp"

namespace gert
{
	class TextNode: public Node
	{
	private:
		void _draw() const;
	public:
		TextNode( const char * t );
		const char * myText;
	};
}
