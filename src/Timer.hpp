#pragma once

namespace gert
{
	class Timer
	{
	private:
		double _max;
		double _start;
	public:
		Timer( double max_seconds );

		void reset();
		double since();
		bool inc_check();
		void mutate_max( double m );
	};
}
