#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <iostream>
#include <random>
#include <ctime>

#include "Draw.hpp"
#include "GameManager.hpp"
#include "gstate/Playing.hpp"
#include "SpellCollector.hpp"

void open_assets();

int main (int argc, char ** argv)
{
	std::atexit ([]{std::cout << std::endl;});
	if (SDL_Init (SDL_INIT_EVERYTHING))
	{
		std::cout << "SDL couldn't initialize\n" << SDL_GetError();
		return EXIT_FAILURE;
	}
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute (SDL_GL_STENCIL_SIZE, 8);

	static constexpr auto window_flags {SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE};
	SDL_Window * window {SDL_CreateWindow ("Goat Tested Colosseum", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, window_flags)};

	const SDL_GLContext windowGLContext {SDL_GL_CreateContext (window)};
	glewExperimental = GL_TRUE;
	const auto glewInitCode {glewInit()};
	if (glewInitCode != GLEW_OK)
	{
		std::cout << "Glew couldn't initialize\n" << glewGetErrorString (glGetError());
		return EXIT_FAILURE;
	}

	glEnable (GL_DEPTH_TEST);

	std::srand (std::time (nullptr));

	SpellCollector foo {"gameData/base/spells.txt"};

	//do game stuff
	try
	{
		open_assets();
		draw_init();

		GameManager mainGame {new state::Playing, window};
		while (mainGame.start());
	}
	catch (const std::exception & e)
	{
		std::cerr << "!!CRASH!! " << e.what() << '\n';
		SDL_ShowSimpleMessageBox (SDL_MESSAGEBOX_ERROR, "Game Crash lmao", e.what(), window);
		return EXIT_FAILURE;
	}

	SDL_GL_DeleteContext (windowGLContext);
	SDL_DestroyWindow (window);
	SDL_Quit();
	return EXIT_SUCCESS;
}
