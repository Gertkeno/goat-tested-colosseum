#pragma once

#include "Node.hpp"
#include "Collision.hpp"
#include "AttributeSet.hpp"

static constexpr auto ACTOR_SPELL_COUNT {8};
class Spell;

// removed Player class in effort to turn Actor into a component hoarder
// just initialize Actor with enough unique properties to warrant an instance instead of a class
// using bytecode for skills should take over the uniqueness constructs would
class Actor
{
protected:
	Node _node;
	void _update_mat();

	const AttributeSet _baseAttribs;
	const float _radius;
	const gert::Texture * const _texture;

	// treat bleed as a second health bar
	float _health, _bleed;
	glm::vec3 _position;

	std::array <int, ACTOR_SPELL_COUNT> _cooldowns;
	unsigned _lastCast;
public:
	Actor (const AttributeSet& as, float r, gert::Texture * t);
	Actor (const Actor &);

	glm::vec3 get_position() const {return _position;}
	Node * get_node() {return &_node;}

	decltype (_health) get_health() const {return _health;}
	decltype (_lastCast) get_last_cast() const {return _lastCast;}
	void take_damage (float dmg);

	bool get_collision (Sphere) const;
	bool get_collision (Pill) const;

	void set_pos_relative (const glm::vec2 & p)
	{
		_position += glm::vec3 {p, 0.0f};
		_update_mat();
	}

	void set_pos_absolute (const glm::vec2 & p)
	{
		_position = glm::vec3 {p, _position.z};
		_update_mat();
	}

	void reduce_cool_down (unsigned index, int turns)
	{
		_cooldowns [index] -= turns;
	}

	void attach_to_node (Node * p)
	{
		p->add_child (&_node);
	}

	virtual float get_attrib (attrib_t i) const {return _baseAttribs [i];}
	virtual const Spell * get_spell (unsigned i) const = 0;
	virtual void cast_spell (unsigned i, Actor *, const glm::vec2 &, int turnCount);
	virtual void basic_attack (Actor * target);
	virtual void turn_update();

	float actionPoints;
	float get_max_action_points() const {return get_attrib (SPEED) + get_attrib (POISE)/2 + 4;}
	float get_max_health() const {return get_attrib (STRENGTH)*2 + 15;}
	float get_reduction() const {return 1.0f / (get_attrib (DEFENSE)/6 + 1.0f);}
};
