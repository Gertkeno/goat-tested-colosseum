#pragma once
#include "SpellCollector.hpp"
#include "Actor.hpp"
#include "Item.hpp"
#include <array>

class Hero: public Actor
{
	using spell_t = const Spell *;
	using SkillTree_t = std::array <std::array <spell_t, 4>, 4>;

	std::array <Item *, 4> _inventory;
	struct
	{
		SkillTree_t * const tree;
		std::array <spell_t, 4> s;
	} _skill;

	void _item_cast (Item::proc_t t, Actor * target)
	{
		for (auto & i : _inventory)
		{
			if (i == nullptr)
				continue;
			if (actionPoints < i->spell->actionPoints or not i->get_proc (t))
				continue;
			actionPoints -= i->spell->actionPoints;
			bc::run (i->spell->code, target, this, {});
		}
	}
public:
	Hero (const AttributeSet &, float, gert::Texture *, SkillTree_t *);
	Hero (const Hero &);

	float get_attrib (attrib_t i) const override;
	const Spell * get_spell (unsigned i) const override;
	void cast_spell (unsigned i, Actor *, const glm::vec2 &, int) override;
	void basic_attack (Actor * target) override;
};
