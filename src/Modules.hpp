#pragma once
#include <string>

class Spell;

namespace modules
{
	bool init();
	const Spell * get_spell (const std::string & subset, const std::string & spellName);
}
