#pragma once
#include "BaseState.hpp"
#include <array>

class Enemy;
using enemy_c = std::array <Enemy *, 8>;

namespace state
{
class Playing: public BaseState
{
	enemy_c _playerTeam;

	glm::vec2 _cursor;
public:
	Playing (enemy_c players);
	[[deprecated]] Playing();
	~Playing();

	BaseState * update (gert::Controller * const c);
};
}
