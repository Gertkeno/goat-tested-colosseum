#include "MainMenu.hpp"

#include "Fighting.hpp"
#include "../ControlList.hpp"

using namespace state;

MainMenu::MainMenu()
{
}

MainMenu::~MainMenu()
{
}

BaseState * MainMenu::update (gert::Controller * const c)
{
	if (c->get_press (control_t::CONFIRM))
		return new Fighting ({nullptr}, {nullptr});
	return nullptr;
}

void MainMenu::draw()
{
}
