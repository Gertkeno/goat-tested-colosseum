#pragma once
#include "BaseState.hpp"
#include "../Node.hpp"
#include <array>

class Spell;
class Actor;
class Hero;
using hero_c = std::array <Hero *, 4>;
class Enemy;
using enemy_c = std::array <Enemy *, 8>;

namespace state
{
class Fighting: public BaseState
{
	Node _root;

	hero_c _player;
	enemy_c _enemy;
	struct
	{
		bool enemy;
		unsigned index;
	} _active;

	glm::vec2 _cursor;

	struct Action
	{
		enum
		{
			MOVEMENT,
			CAST_TO,
			NONE
		} type;

		int spellIndex;
		Actor * target;
	} _action;
	const Spell * _spellTip;

	bool _apply_action (Actor * const caster) const;
	Actor * _active_actor() const;
	int _turnCount;
public:
	Fighting (hero_c playerTeam, enemy_c enemyTeam);

	BaseState * update (gert::Controller * const);
	void draw();
};
}
