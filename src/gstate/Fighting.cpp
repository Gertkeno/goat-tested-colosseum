#include "Fighting.hpp"
#include "MainMenu.hpp"
#include "../Hero.hpp"
#include "../Enemy.hpp"
#include "../ControlList.hpp"
#include "../GameExcept.hpp"

using namespace state;

Fighting::Fighting (hero_c player, enemy_c enemy)
: _player {player}
, _enemy {enemy}
, _active {true, 0}
, _action {Action::NONE, -1, nullptr}
, _spellTip {nullptr}
, _turnCount {0}
{
	[this] (auto t)
	{
		for (auto & i: t)
		{
			if (i != nullptr)
				i->attach_to_node (&_root);
		}
	} (_enemy), (_player);
}

inline int spell_hot_key_get (const gert::Controller * const c)
{
	if (c->get_press (control_t::SPELL_1))
		return 0;
	else if (c->get_press (control_t::SPELL_2))
		return 1;
	else if (c->get_press (control_t::SPELL_3))
		return 2;
	else if (c->get_press (control_t::SPELL_4))
		return 3;
	else if (c->get_press (control_t::SPELL_5))
		return 4;
	else if (c->get_press (control_t::SPELL_6))
		return 5;
	else if (c->get_press (control_t::SPELL_7))
		return 6;
	else if (c->get_press (control_t::SPELL_8))
		return 7;
	return -1;
}

BaseState * Fighting::update (gert::Controller * const c)
{
	{
		const glm::vec4 t {gert::Controller::get_mouse_normal(), 1.0f, 1.0f};
		_cursor = glm::vec2 (glm::inverse (_root.get_calc()) * t);
	}

	const auto set_spell_index {[&] (int i)
	{
		if (i == -1)
			_spellTip = nullptr;
		else
			_spellTip = _active_actor()->get_spell (i);
		_action.spellIndex = i;
	}};

	//_cursor.pos +=
	if (c->get_press (control_t::CONFIRM) and _action.type != Action::NONE)
	{
		if (_apply_action (_active_actor()) and _active_actor()->actionPoints <= 0.0f)
		{
			++_turnCount;
			++_active.index;
			// assuming both teams are sorted and end at nullptr
			_action.target = nullptr;
			if (_active_actor() == nullptr)
			{
				_active.enemy = not _active.enemy;
				_active.index = 0;
			}
		}
		else
		{
			_action.type = Action::MOVEMENT;
			// null stuff should be handled by _apply_action
		}
	}
	else if (const int hotKeyCheck = spell_hot_key_get (c) != -1)
	{
		try
		{
			if (_action.spellIndex != -1 and hotKeyCheck != _action.spellIndex)
			{
				// apply deselect animation
			}
			set_spell_index (hotKeyCheck);
		}
		catch (const ActorError & e)
		{
			std::cout << "Failed to select spell " << hotKeyCheck << " because " << e.what() << '\n';
		}
	}
	else if (c->get_press (control_t::CANCEL))
	{
		if (_action.spellIndex != -1)
		{
			// apply deselect animation to _action.spellIndex
			set_spell_index (-1);
		}
		_action.type = Action::MOVEMENT;
	}

	// team wipe check
	auto downed {0u};
	for (auto & i : _player)
	{
		if (i == nullptr)
			++downed;
		else if (i->get_health() <= 0)
			++downed;
	}
	if (downed == _player.size())
		new MainMenu;// fallen
	return nullptr;
}

void Fighting::draw()
{
	_root.render();
}

bool Fighting::_apply_action (Actor * const caster) const
{
	switch (_action.type)
	{
	case Action::CAST_TO:
		try
		{
			caster->cast_spell (_action.spellIndex, _action.target, _cursor, _turnCount);
		}
		catch (const ActorError & e)
		{
			std::cout << "Actor couldn't cast spell " << e.what() << '\n';
			return false;
		}
		break;
	case Action::MOVEMENT:{
		const glm::vec2 a {caster->get_position()};
		const auto minsetter {std::min (caster->actionPoints, 1.0f)};
		const auto dist {std::max (minsetter, glm::distance (a, _cursor))};
		if (caster->actionPoints >= dist)
		{
			caster->set_pos_absolute (_cursor);
			caster->actionPoints -= dist;
		}
		else
			return false;
		}break;
	case Action::NONE:
		return false;
	}
	return true;
}

Actor * Fighting::_active_actor() const
{
	if (_active.enemy)
	{
		if (_active.index >= _enemy.size())
			return nullptr;
		else
			return _enemy [_active.index];
	}
	else
	{
		if (_active.index >= _player.size())
			return nullptr;
		else
			return _player [_active.index];
	}
}
