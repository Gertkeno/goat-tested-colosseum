#include "Playing.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include "../Enemy.hpp"
#include "../AssetList.hpp"
#include "../FrameTime.hpp"
#include "../ControlList.hpp"
#include "../DebugOut.hpp"
#include "../GameExcept.hpp"

using namespace state;
namespace
{
	constexpr float MAX_CURSOR_SPEED {2.0f};
}

Playing::Playing (enemy_c player)
: _playerTeam {player}
{
	_playerTeam [0] = new Enemy {AttributeSet {1}, 1.0f, gTexture + texture_t::DEBUG_CIRCLE, Enemy::spell_c {nullptr}, 999};
	_playerTeam [0]->actionPoints = _playerTeam [0]->get_max_action_points();
	_playerTeam [0]->attach_to_node (&_root);
}

Playing::Playing() : Playing (enemy_c {nullptr}) {}
Playing::~Playing() {}

BaseState * Playing::update (gert::Controller * const c)
{
	// cursor update?
	/*const float speed (MAX_CURSOR_SPEED * FrameTime::get_mod());
	const auto & co {*c};
	const float x {speed * (co [control_t::RIGHT] - co [control_t::LEFT])};
	const float y {speed * (co [control_t::UP] - co [control_t::DOWN])};
	_cursor += glm::vec2 {x, y};*/

	const glm::vec4 t {gert::Controller::get_mouse_normal(), 1.0f, 1.0f};
	_cursor = glm::vec2 (glm::inverse (_root.get_calc()) * t);

	(void) c;
	//static int turncount = 1;
	/*try
	{
		_playerTeam [0]->cast_spell (0, nullptr, _cursor, turncount++);
	}
	catch (const ActorError & e)
	{
		std::cout << "who cares " << e.what() << '\n';
	}*/
	_playerTeam [0]->actionPoints = _playerTeam [0]->get_max_action_points();
	//spell::teleport.run (_playerTeam [0], _playerTeam [0], _cursor);
	//_byteRunner.run_code (testCode, _playerTeam [0], nullptr, _cursor);

	return nullptr;
}
