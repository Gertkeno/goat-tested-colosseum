#pragma once

#include "BaseState.hpp"

namespace state
{
	class MainMenu: public BaseState
	{
	public:
		MainMenu();
		~MainMenu();

		BaseState * update( gert::Controller * const c );
		void draw();
	protected:
	};
}
