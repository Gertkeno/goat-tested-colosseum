#pragma once
#include "../Node.hpp"

namespace gert {class Controller;}
namespace state
{
class BaseState
{
protected:
	Node _root;
public:
	BaseState() {}
	virtual ~BaseState() {}
	virtual BaseState * update (gert::Controller * const c) = 0;

	virtual void draw();
	virtual void update_aspect (float w, float h);
};
}
