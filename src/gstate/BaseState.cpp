#include "BaseState.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace state;
void BaseState::draw()
{
	_root.render();
}

void BaseState::update_aspect(float w, float h)
{
	const float max {std::max (w,h)};
	const auto scale {glm::scale (glm::mat4(), glm::vec3 {h/max, w/max, 1.0f})};
	_root.edit_mat (scale);
}
