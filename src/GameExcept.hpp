#pragma once
#include <stdexcept>

class Actor;

class GameError: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

struct ActorError: public std::runtime_error
{
	const Actor * const context;
	ActorError (const Actor * p, std::string s)
	: std::runtime_error (s)
	, context {p}
	{}
};
