#include "TextDraw.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Draw.hpp"
#include "AssetList.hpp"
#include <cctype>

static constexpr float letter_width {0.0256410256f};

void TextDraw (const char * text, const glm::mat4 & pos, glm::vec3 c)
{
	static const auto font_shape {glm::scale( glm::mat4(), {0.5f,1.0f,1.0f} )};

	auto mat {pos * font_shape};
	for (auto p{text}; *p != '\0'; ++p)
	{
		glm::vec4 slice {0.0f, 0.0f, letter_width, 1.0f};
		const auto numTest {*p - '0'};
		const auto alTest {std::toupper(*p) - 'A'};
		bool noDraw {false};
		if (numTest >= 0 and numTest <= 9)
			slice.x = numTest+26;
		else if (alTest >= 0 and alTest < 26)
			slice.x = alTest;
		else if (*p == '!')
			slice.x = 36;
		else if (*p == '?')
			slice.x = 37;
		else if (*p == '$')
			slice.x = 38;
		else
			noDraw = true;

		if (not noDraw)
		{
			slice.x *= letter_width;
			Draw (mat, gTexture+texture_t::FONT_0, slice, c);
		}
		mat = glm::translate (mat, glm::vec3 {2.0f, 0.0f, 0.0f});
	}
}
