#include "TextNode.hpp"

#include "TextDraw.hpp"

using namespace gert;

TextNode::TextNode( const char * t )
: Node()
, myText (t)
{}

void TextNode::_draw() const
{
	TextDraw( myText, get_calc(), myColor );
}
