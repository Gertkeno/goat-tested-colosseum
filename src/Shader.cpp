#include "Shader.hpp"

#include <SDL2/SDL_rwops.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtc/type_ptr.hpp>
#include <stdexcept>

#include <iostream>

using namespace gert;

Shader* Shader::Active_Shader = nullptr;

void Shader::_init (SDL_RWops * rw, shade_t t)
{
	assert (rw != nullptr);

	const auto SHADER_SIZE {SDL_RWsize (rw)};
	GLchar * const textBuffer {new GLchar [SHADER_SIZE + 1]};
	SDL_RWread (rw, textBuffer, sizeof (GLchar), SHADER_SIZE);
	textBuffer [SHADER_SIZE] = '\0';
	SDL_RWclose (rw);

	// compile
	_shaders [t] = glCreateShader (GL_FRAGMENT_SHADER + t);
	glShaderSource (_shaders [t], 1, &textBuffer, nullptr);
	glCompileShader (_shaders [t]);

	delete[] textBuffer;

	GLint status;
	glGetShaderiv (_shaders [t], GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
	{
		int len;
		glGetShaderiv (_shaders [t], GL_INFO_LOG_LENGTH, &len);
		std::string err {"[Shader] error making shader\n"};
		assert (len > 0);
		char * text = new char [len];
		glGetShaderInfoLog (_shaders [t], len, nullptr, text);
		err.append (text);
		delete[] text;
		throw std::runtime_error {err};
	}

	// attach and link
	_program = glCreateProgram();
	glAttachShader (_program, _shaders [FRAGMENT]);
	glAttachShader (_program, _shaders [VERTEX]);

	glLinkProgram (_program);
}

Shader::Shader (const char * ffile, const char * vfile)
{
	_init (SDL_RWFromFile (ffile, "rb"), FRAGMENT);
	_init (SDL_RWFromFile (vfile, "rb"), VERTEX);
}

Shader::Shader (const void * fdata, unsigned fsize, const void * vdata, unsigned vsize)
{
	_init (SDL_RWFromConstMem (fdata, fsize), FRAGMENT);
	_init (SDL_RWFromConstMem (vdata, vsize), VERTEX);
}

Shader::~Shader()
{
	glDeleteProgram (_program);
	glDeleteShader (_shaders [FRAGMENT]);
	glDeleteShader (_shaders [VERTEX]);
	if (Active_Shader == this)
		Active_Shader = nullptr;
}

void Shader::use_shader (void)
{
	if (Active_Shader == this)
		return;

	glUseProgram (_program);
	Active_Shader = this;
}

GLuint Shader::get_program (void)
{
	return _program;
}

void Shader::set_uniform (const std::string& title, float value)
{
	glProgramUniform1f (_program, get_location (title), value);
}

void Shader::set_uniform (const std::string& title, int value)
{
	glProgramUniform1i (_program, get_location (title), value);
}

void Shader::set_uniform (const std::string& title, GLuint value)
{
	glProgramUniform1ui (_program, get_location (title), value);
}

void Shader::set_uniform (const std::string& title, glm::vec3 value)
{
	glProgramUniform3fv (_program, get_location (title), 1, glm::value_ptr (value));
}

void Shader::set_uniform (const std::string& title, glm::vec4 value)
{
	glProgramUniform4fv (_program, get_location (title), 1, glm::value_ptr (value));
}

int Shader::get_location (const std::string& title)
{
	if (_recentUniform.count(title) == 0u)
	{
		_recentUniform[title] = glGetUniformLocation (_program, title.c_str());
		//std::cout << "[Shader]  Made new map entry, key " << title << " and value " << _recentUniform[title] << '\n';
	}
	return _recentUniform[title];
}
