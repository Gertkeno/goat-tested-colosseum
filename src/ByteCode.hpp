#pragma once

#include <vector>
#include <glm/glm.hpp>

class Actor;

namespace bc
{
enum bytecode_t
{
	END,
	PL, // PUSH_LITERAL
	TARGET,
	CASTER,

	// actor get
	PUSH_POS,
	PUSH_CURSOR,
	PUSH_ATTRIB,

	// actor set
	SET_POS_REL,
	SET_POS_ABS,
	DAMAGE,

	// mathematics
	ADD,
	SUB,
	MULTIPLY,
	DIVIDE,
	SUB_VEC,
	NORMAL_VEC,

	NEGATIVE,
};

bytecode_t str_to_code (const char * str);

union key
{
	float value;
	bytecode_t word;

	constexpr key (float v): value {v} {}
	constexpr key (bytecode_t w): word {w} {}
	key(){}
};

void run (const bc::key *, Actor * target, Actor * caster, glm::vec2 cursor);
}
