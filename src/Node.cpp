#include "Node.hpp"

#include "Draw.hpp"

Node::Node()
: _dirty {true}
, myTexture {nullptr}
, mySlice {0.0f, 0.0f, 1.0f, 1.0f}
, myColor {1.0f, 1.0f, 1.0f}
{}

Node::~Node()
{}

void Node::render (const glm::mat4& parent, bool dirty)
{
	dirty = dirty or _dirty;
	if (dirty)
	{
		_calc = parent * _mat;// * parent;
		_dirty = false;
	}

	if (myTexture != nullptr)
		_draw();

	for (auto & i : _children)
		i->render (_calc, dirty);
}

glm::vec3 Node::position() const
{
	return {_mat[3][0], _mat[3][1], _mat[3][2]};
}

Node * Node::add_child (Node * c)
{
	assert (c != nullptr);
	_children.push_back (c);
	return c;
}

bool Node::remove_child (Node * c)
{
	assert (c != nullptr);
	for (auto i = _children.begin(); i != _children.end(); ++i)
	{
		if (c != *i)
			continue;
		_children.erase (i);
		return true;
	}
	return false;
}

void Node::_draw() const
{
	Draw (_calc, myTexture, mySlice, myColor);
}

glm::mat4 Node::get_calc() const
{
	return _calc;
}

glm::mat4 Node::get_relative() const
{
	return _mat;
}

void Node::edit_mat (const glm::mat4 & m)
{
	_dirty = true;
	_mat = m;
}
