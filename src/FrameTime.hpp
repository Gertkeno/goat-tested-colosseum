#pragma once

namespace FrameTime
{
	double get_pure();
	double get_mod();
	void set_mod( double set );

	void update();
	int since_update();
}
