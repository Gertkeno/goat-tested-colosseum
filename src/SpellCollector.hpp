#pragma once
#include "ByteCode.hpp"
#include <string>
#include <climits>

struct Spell
{
	bc::key * code;
	float range, actionPoints, cooldown;
	enum: unsigned char
	{
		SELF   = 1 << 0,
		ALLY   = 1 << 1,
		ENEMY  = 1 << 2,
		GROUND = 1 << 3,
		ANY    = UCHAR_MAX,
	};
	unsigned char target;
	std::string title, description;
private:
	friend class SpellCollector;
	Spell(): code {nullptr}, target {0} {}
};

class SpellCollector
{
	std::vector <Spell> _collection;
public:
	SpellCollector (const std::string &);
	~SpellCollector();

	const Spell * operator[] (int i) const {return & _collection [i];}
	const Spell * find_name (const std::string & name) const;
};
