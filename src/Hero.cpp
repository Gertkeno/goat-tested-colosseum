#include "Hero.hpp"
#include "GameExcept.hpp"

Hero::Hero (const AttributeSet & a, float r, gert::Texture * t, SkillTree_t * skt)
: Actor (a, r, t)
, _inventory {nullptr}
, _skill {skt, nullptr}
{
}

Hero::Hero (const Hero & h)
: Actor (h)
, _inventory {h._inventory}
, _skill {h._skill}
{
}

float Hero::get_attrib (attrib_t i) const
{
	auto atc {_baseAttribs [i]};
	for (auto & x : _inventory)
	{
		if (x == nullptr)
			continue;
		atc += x->attributes [i];
	}
	return atc;
}

const Spell * Hero::get_spell (unsigned i) const
{
	if (i > 4)
	{
		const auto index {i-4};
		if (_inventory [index] == nullptr)
			throw ActorError {this, "No item here."};
		const auto s {_inventory [index]->spell};
		if (s == nullptr)
			throw ActorError {this, "Item without spell."};
		if (not _inventory [index]->get_proc (Item::USE))
			throw ActorError {this, "Item spell is not on-use."};
		return s;
	}
	const auto s {_skill.s [i]};
	if (s == nullptr)
		throw ActorError {this, "No skill at " + std::to_string (i) + '.'};
	return s;
}

void Hero::cast_spell (unsigned i, Actor * target, const glm::vec2 & cursor, int tc)
{
	Actor::cast_spell (i, target, cursor, tc);
	if (target != nullptr)
		_item_cast (Item::ON_CAST, target);
}

void Hero::basic_attack (Actor * target)
{
	Actor::basic_attack (target);
	_item_cast (Item::ATTACK, target);
}
