#pragma once
#include "AttributeSet.hpp"

class Spell;

class Item
{
public:
	AttributeSet attributes;
	const Spell * spell;
	const float level;
	enum proc_t
	{
		USE,
		ATTACK,
		ON_CAST,
		ANY,
	};

	constexpr bool get_proc (proc_t t) {return (_proc & t) > 0;}

	// ctor
	constexpr Item (const AttributeSet & a, Spell * s, float l, proc_t p)
	: attributes {a}
	, spell {s}
	, level {l}
	, _proc {p}
	{}

	constexpr Item (const AttributeSet & a, Spell * s, float l): Item (a, s, l, proc_t::USE) {}
private:
	proc_t _proc;
};
