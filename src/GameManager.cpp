#include "GameManager.hpp"

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_events.h>

#include <algorithm>

#include "FrameTime.hpp"
#include "ControlList.hpp"
#include "gstate/BaseState.hpp"
#include "Node.hpp"

static constexpr gert::cunit cdata[]{
	{control_t::CONFIRM, {gert::Input::type_t::KEYBOARD, SDLK_z}},
	{control_t::CANCEL,  {gert::Input::type_t::KEYBOARD, SDLK_ESCAPE}},
	{control_t::DOWN,    {gert::Input::type_t::KEYBOARD, SDLK_DOWN}},
	{control_t::LEFT,    {gert::Input::type_t::KEYBOARD, SDLK_LEFT}},
	{control_t::RIGHT,   {gert::Input::type_t::KEYBOARD, SDLK_RIGHT}},
	{control_t::UP,      {gert::Input::type_t::KEYBOARD, SDLK_UP}},
	{control_t::SPELL_1, {gert::Input::type_t::KEYBOARD, SDLK_1}},
	{control_t::SPELL_2, {gert::Input::type_t::KEYBOARD, SDLK_2}},
	{control_t::SPELL_3, {gert::Input::type_t::KEYBOARD, SDLK_3}},
	{control_t::SPELL_4, {gert::Input::type_t::KEYBOARD, SDLK_4}},
	{control_t::SPELL_5, {gert::Input::type_t::KEYBOARD, SDLK_5}},
	{control_t::SPELL_6, {gert::Input::type_t::KEYBOARD, SDLK_6}},
	{control_t::SPELL_7, {gert::Input::type_t::KEYBOARD, SDLK_7}},
	{control_t::SPELL_8, {gert::Input::type_t::KEYBOARD, SDLK_8}},
};

GameManager::GameManager (state::BaseState * p, SDL_Window * win)
: _state {p}
, _window {win}
, _controls {new gert::Controller (cdata, control_t::TOTAL)}
{
	assert (p != nullptr);
	_update_aspect();
}

GameManager::~GameManager()
{
	delete _controls;
}

bool GameManager::start()
{
	FrameTime::update();
	_controls->reset_press();
	while (SDL_PollEvent (&_event))
	{
		switch (_event.type)
		{
		case SDL_JOYDEVICEADDED:
			_controls->joy_add (_event.jdevice.which);
			break;
		case SDL_JOYDEVICEREMOVED:
			_controls->joy_remove (_event.jdevice.which);
			break;
		case SDL_KEYDOWN:
			if (_event.key.keysym.sym == SDLK_q and (SDL_GetModState() & KMOD_CTRL) != 0)
				return false;
			break;
		case SDL_QUIT:
			return false;
		case SDL_WINDOWEVENT:
			if (_event.window.event == SDL_WINDOWEVENT_RESIZED)
				_update_aspect();
			break;
		}
		_controls->manage_inputs (&_event);
	}
	const auto newState {_state->update (_controls)};
	if (newState != nullptr)
	{
		delete _state;
		_state = newState;
	}

	glClearColor (0.8f, 0.8f, 0.8f, 1.0f);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	_state->draw();

	SDL_GL_SwapWindow (_window);
	static constexpr int MAX_FPS {1000/60};
	SDL_Delay (std::max (0, MAX_FPS - FrameTime::since_update()));
	return true;
}

void GameManager::_update_aspect()
{
	int w, h;
	SDL_GetWindowSize (_window, &w, &h);
	gert::Controller::set_window_wh (w, h);
	glViewport (0, 0, w, h);
	_state->update_aspect (w, h);
	/*const float max (std::max (w,h));
	const auto scale {glm::scale (glm::mat4(), glm::vec3 {h/max, w/max, 1.0f})};
	glUniformMatrix4fv (10, 1, GL_FALSE, glm::value_ptr (scale));*/
}
