#pragma once
#include <glm/glm.hpp>

struct Sphere
{
	glm::vec3 pos;
	float radius;
};


struct Pill
{
	glm::vec3 a, b;
	float radius;
};
