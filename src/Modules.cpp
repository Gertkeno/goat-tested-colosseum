#include "Modules.hpp"
#include "SpellCollector.hpp"
#include "ActorCollector.hpp"

#include <fstream>
#include <stdexcept>
#include <vector>
#include <map>

namespace
{
	struct Module
	{
		SpellCollector * spell;
		ActorCollector * enemy;
	};

	std::map <std::string, Module> _modules;
}

namespace modules
{
bool init()
{
	std::ifstream moduleList {"gameData/list.txt"};
	std::vector <std::string> moduleNames;
	do
	{
		std::string name;
		moduleList >> name;
		moduleNames.push_back (std::move (name));
	} while (not moduleList.eof());

	static const auto dirPath {[] (const std::string & a, const std::string & n)
	{
		return "gameData/" + a + '/' + n;
	}};

	// order matters
	for (auto & i : moduleNames)
		_modules [i].spell = new SpellCollector (dirPath (i, "spells.txt"));
	for (auto & i : moduleNames)
		_modules [i].enemy = new ActorCollector (dirPath (i, "enemies.txt"));

	return true;
}

const Spell * get_spell (const std::string & subset, const std::string & spellName)
{
	if (_modules.count (subset) == 0)
		throw std::runtime_error {"Couldn't find subset \"" + subset + '"'};

	try
	{
		return _modules.at (subset).spell->find_name (spellName);
	}
	catch (const std::runtime_error & e)
	{
		throw std::runtime_error {"In subset \"" + subset + "\": " + e.what()};
	}
}
}
