#include "Controller.hpp"

#include "DebugOut.hpp"

#include <SDL2/SDL.h>
#include <fstream>
#include <algorithm>
#include <cassert>

using namespace gert;

namespace
{
	//normalized values
	constexpr float AXIS_PRESS_THRESHOLD {0.8f};
	constexpr float AXIS_RESET_THRESHOLD {0.7f};
	constexpr float AXIS_DEAD_ZONE {0.039f};

	constexpr void digital_press (Input* t)
	{
		if (t->held < 1.0f)
			t->framePress = true;
		t->held = 1.0f;
		//std::cerr << "Pressed button " << t->button << std::endl;
	}

	constexpr void digital_release (Input* t)
	{
		t->held = 0.0f;
	}
}

glm::vec2 Controller::_mouseDivisor {1, 1};
glm::vec2 Controller::_mouseCalc {0, 0};

Controller::Controller (const char * filename)
{
	std::ifstream in (filename);
	if (not in.is_open())
		throw std::runtime_error {"Couldn't open controller file \"" + std::string (filename) + '"'};

	decltype (_data.size()) index {0u};
	in >> index;
	in.get();
	while (index > 0 and not in.eof())
	{
		Input temp;
		in >> temp;
		_data.emplace_back (std::move (temp));
		--index;
	}
}

Controller::Controller (const cunit * data, size_t s)
: _data {s}
{
	for (auto a {0u}; a < s; ++a)
		_data[data[a].first] = data[a].second;
}

constexpr float mouse_math (int pos, float wh)
{
	return (pos / wh) * 2 - 1;
}

void Controller::manage_inputs (const SDL_Event* e)
{
	auto digital_get {digital_release};
	switch (e->type)
	{
	// digital press/releases
	case SDL_JOYBUTTONDOWN:
		digital_get = digital_press;
	case SDL_JOYBUTTONUP:
		// only joysticks need to be tested, return if not tracked
		if (not _tracked_joystick (e->jbutton.which))
			break;
		_search_input (Input::type_t::JOYBUTTON, e->jbutton.button, digital_get);

	case SDL_KEYDOWN:
		digital_get = digital_press;
	case SDL_KEYUP:
		_search_input (Input::type_t::KEYBOARD, e->key.keysym.sym, digital_get);
		break;

	case SDL_MOUSEBUTTONDOWN:
		digital_get = digital_press;
	case SDL_MOUSEBUTTONUP:
		_search_input (Input::type_t::MOUSE, e->button.button, digital_get);
		break;

	// axis
	case SDL_JOYAXISMOTION:
		if (not _tracked_joystick (e->jaxis.which))
			break;
		_search_input (Input::type_t::JOYAXIS, e->jaxis.axis, [e](Input* t)
		{
			t->held = static_cast<float>(e->jaxis.value) / t->axisMax;
			if (t->held < AXIS_DEAD_ZONE)
				t->held = 0.0f;
			else if (t->held > 1.0f)
				t->held = 1.0f;

			if (t->axisPass and t->held < AXIS_RESET_THRESHOLD)
				t->axisPass = false;
			else if (not t->axisPass and t->held > AXIS_PRESS_THRESHOLD)
			{
				t->framePress = true;
				t->axisPass = true;
			}
		});
		break;

	case SDL_MOUSEMOTION:
		_mouseCalc = {mouse_math (e->motion.x, _mouseDivisor.x), -mouse_math (e->motion.y, _mouseDivisor.y)};
		//DE_OUT (e->motion.xrel << ", " << e->motion.yrel);
		break;
	}
}

void Controller::reset_press()
{
	for (auto & i: _data)
		i.framePress = false;
}

void Controller::reset_held()
{
	for (auto & i: _data)
		i.held = 0.0f;
}

bool Controller::save_file (const char * filename) const
{
	if (filename == nullptr)
		return false;
	std::ofstream out (filename);
	if (not out.is_open())
	{
		DE_OUT ("[Controller]  Couldn't open file " << filename);
		return false;
	}

	out << _data.size();
	out << '\n';
	for (auto & i: _data)
		out << i;
	return true;
}

bool Controller::mutate_value (size_t key, const Input& val)
{
	assert (key < _data.size());

	_data[key] = val;
	reset_held();
	reset_press();
	return true;
}

bool Controller::_tracked_joystick (SDL_JoystickID foo)
{
	for (auto & i: _joys)
		if (SDL_JoystickInstanceID (i) == foo) return true;
	return false;
}

bool Controller::joy_add (int device_index)
{
	auto foo {SDL_JoystickOpen (device_index)};
	if (foo == nullptr)
		return false;
	_joys.push_back (foo);
	DE_OUT ("[Controller]  Opened joystick " << device_index << '\t' << SDL_JoystickName (foo));
	reset_held();
	return true;
}

bool Controller::joy_remove (SDL_JoystickID instance_id)
{
	for (auto i = 0u; i < _joys.size(); ++i)
	{
		if (instance_id != SDL_JoystickInstanceID (_joys[i]))
			continue;
		DE_OUT ("[Controller]  Close joystick " << instance_id << '\t' << SDL_JoystickName (_joys[i]));
		SDL_JoystickClose (_joys[i]);
		_joys.erase (_joys.begin()+i);
		reset_held();
		return true;
	}
	return false;
}

void Controller::debug_draw (void(*d)(int, float))
{
	for (auto i {0u}; i < _data.size(); ++i)
		d (i, _data [i].held);
}

namespace gert
{
std::ostream & operator << (std::ostream & out, const Input & i)
{
	out << static_cast <int> (i.myType);
	out << ' ';
	out << i.button;
	out << ' ';
	out << i.axisMax;

	out << '\n';
	return out;
}

std::istream & operator >> (std::istream & in, Input & i)
{
	int type;
	in >> type;
	i.myType = static_cast <Input::type_t> (type);
	in >> i.button;
	in >> i.axisMax;

	return in;
}
} // gert
