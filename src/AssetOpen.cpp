#include "AssetList.hpp"

#include "Texture.hpp"
#include "Shader.hpp"
#include <iostream>

gert::Shader * gShaders;
gert::Texture * gTexture;

void open_assets()
{
	gert::Texture::linear_filter = false;

	gShaders = new gert::Shader {"assets/glsl/basic.frag", "assets/glsl/basic.vert"};
	gShaders->use_shader();

	gTexture = new gert::Texture[texture_t::TOTAL]
	{
		{"assets/smiley.bmp"},
		{"assets/checkerCircle.bmp"},
		{"assets/font.bmp"},
	};
}
