#pragma once
#include <cstddef>
#include <string>

struct SDL_RWops;
using GLuint = unsigned int;

//BMP only

namespace gert
{
class Shader;

class Texture
{
public:
	Texture (const char * filename);
	Texture (const void * mem, unsigned size);
	~Texture();

	void open_file (const char *filename);
	void set_active (GLuint specific = 0) const;

	unsigned get_width() const {return _width;}
	unsigned get_height() const {return _height;}

	bool is_file (const std::string & s) const;
	static bool linear_filter;
protected:
	void _init (SDL_RWops * rw);
	void _set_parameters();

	GLuint _textureIndex;
	unsigned _width, _height;
	std::string _fileName;
};
}// gert
