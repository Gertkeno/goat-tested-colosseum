#include "Enemy.hpp"
#include <cassert>
#include "SpellCollector.hpp"
#include "GameExcept.hpp"

Enemy::Enemy (const AttributeSet & as, float r, gert::Texture * t, const spell_c & sc, float l)
: Actor (as, r, t)
, _spells {sc}
, _level {l}
{
}

Enemy::Enemy (const Enemy & ec)
: Actor (ec)
, _spells {ec._spells}
, _level {ec._level}
{
}

const Spell * Enemy::get_spell (unsigned i) const
{
	assert (i < _spells.size());
	auto r {_spells [i]};
	if (r == nullptr)
		throw ActorError {this, "No spell here."};
	return r;
}
