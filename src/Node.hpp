#pragma once

#include <glm/glm.hpp>
#include <vector>

namespace gert {class Texture;}

class Node
{
private:
	std::vector<Node *> _children;
	glm::mat4 _mat;
	glm::mat4 _calc;
	bool _dirty;
protected:
	virtual void _draw() const;
public:
	Node();
	virtual ~Node();

	const gert::Texture * myTexture;
	glm::vec4 mySlice;
	glm::vec3 myColor;

	void edit_mat (const glm::mat4 &);
	glm::mat4 get_relative() const;

	void render (const glm::mat4& = glm::mat4(), bool = false);
	glm::mat4 get_calc() const;
	glm::vec3 position() const;
	Node * add_child (Node *);
	bool remove_child (Node *);
};
