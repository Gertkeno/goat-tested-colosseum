#pragma once

#include "Actor.hpp"

class Dummy: public Actor
{
public:
	Dummy(): Actor (AttributeSet (2), 4.56, nullptr) {}

	const Spell * get_spell (unsigned i) const override {(void)i; return nullptr;}
};
