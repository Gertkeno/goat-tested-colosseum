#pragma once
#include <functional>

enum attrib_t
{
	POISE,    // AP+, stagger defense
	SPEED,    // movement per AP
	STRENGTH, // health+, damage in melee
	MAGIC,    // exclusively used in spells?
	DEFENSE,  // damage reduction
	TOTAL
};

class AttributeSet
{
	float v[TOTAL];

	template <typename Operator>
	static constexpr AttributeSet accumulate (const AttributeSet & l, const AttributeSet & r)
	{
		AttributeSet temp;
		for (auto i {0u}; i < TOTAL; ++i)
			temp.v [i] = Operator{} (l.v [i], r.v[i]);
		return temp;
	}

public:
	constexpr AttributeSet (float p, float sd, float str, float m, float d): v {p, sd, str, m , d} {}
	constexpr AttributeSet (float a = 0): AttributeSet (a, a, a, a, a) {}

	constexpr float & operator[] (attrib_t i) {return v [i];}
	constexpr float operator[] (attrib_t i) const {return v [i];}

	constexpr AttributeSet operator+ (const AttributeSet & r)
	{
		return accumulate <std::plus <float>> (*this, r);
	}

	constexpr AttributeSet operator* (const AttributeSet & r)
	{
		return accumulate <std::multiplies <float>> (*this, r);
	}

	constexpr AttributeSet operator- (const AttributeSet & r)
	{
		return accumulate <std::minus <float>> (*this, r);
	}

	constexpr AttributeSet operator/ (const AttributeSet & r)
	{
		return accumulate <std::divides <float>> (*this, r);
	}
};
