#pragma once
#include "Texture.hpp"

namespace texture_t
{
	enum: char
	{
		FACE,
		DEBUG_CIRCLE,
		FONT_0,
		TOTAL
	};
}

extern gert::Texture * gTexture;
