#include "Texture.hpp"

#include <SDL2/SDL_surface.h>
#include <GL/glew.h>

#include "DebugOut.hpp"

using namespace gert;
bool Texture::linear_filter = true;

void Texture::_init (SDL_RWops * ptr)
{
	const auto surf {SDL_LoadBMP_RW (ptr, true)};
	if (surf == nullptr)
		throw std::runtime_error {"Couldn't create temporary surfrace from memory: " + std::string (SDL_GetError())};

	const int mode {[](const int & i)
	{
		if (i == 3)
			return GL_RGB;
		else if (i == 1)
			return GL_RED;
		return GL_RGBA;
	}(surf->format->BytesPerPixel)};

	this->_width = surf->w;
	this->_height = surf->h;

	glBindTexture (GL_TEXTURE_2D, _textureIndex);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB8, surf->w, surf->h, 0, mode, GL_UNSIGNED_BYTE, surf->pixels);

	SDL_FreeSurface (surf);
	_set_parameters();
}

Texture::Texture (const char * filename)
: _fileName (filename)
{
	glGenTextures (1, &_textureIndex);
	_init (SDL_RWFromFile (filename, "rb"));
}

Texture::Texture (const void * mem, unsigned size)
: _fileName ("RAW")
{
	glGenTextures (1, &_textureIndex);
	_init (SDL_RWFromConstMem (mem, size));
}

Texture::~Texture()
{
	glDeleteTextures (1, &_textureIndex);
}

void Texture::open_file (const char * filename)
{
	_init (SDL_RWFromFile (filename, "rb"));
	_fileName = filename;
}

void Texture::set_active (GLuint specific) const
{
	glActiveTexture (GL_TEXTURE0 + specific);
	glBindTexture (GL_TEXTURE_2D, _textureIndex);

	glUniform1i (20+specific, specific);
}

void Texture::_set_parameters()
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	const auto filter {linear_filter ? GL_LINEAR : GL_NEAREST};
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
}

bool Texture::is_file (const std::string & s) const
{
	return _fileName == s;
}
