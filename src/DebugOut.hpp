#ifndef NDEBUG
#include <iostream>
#define DE_OUT(x) std::cout << x << '\n'
#else
#define DE_OUT(x) ;
#endif
