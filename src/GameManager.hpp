#pragma once
#include <SDL2/SDL_events.h>

struct SDL_Window;

namespace state {class BaseState;}
namespace gert {class Controller;}

class GameManager
{
private:
	state::BaseState * _state;
	SDL_Event _event;
	SDL_Window * const _window;

	gert::Controller * _controls;
	void _update_aspect();
public:
	GameManager (state::BaseState *, SDL_Window *);
	~GameManager();

	bool start();
};
