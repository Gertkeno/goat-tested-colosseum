#pragma once

#include "Enemy.hpp"
#include <vector>
#include "Texture.hpp"

class ActorCollector
{
	struct ActorParent
	{
		AttributeSet as;
		float radius;
		std::string name, texturefile;
		Enemy::spell_c spell {nullptr};
	};
	std::vector <gert::Texture> _activeTextures;
	std::vector <ActorParent> _collection;
public:
	ActorCollector (const std::string & filename);

	const gert::Texture * get_texture (unsigned i) const;
};
