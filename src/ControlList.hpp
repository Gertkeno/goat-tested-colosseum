#pragma once
namespace control_t
{
	enum: char
	{
		UP,
		DOWN,
		LEFT,
		RIGHT,
		CONFIRM,
		CANCEL,
		SPELL_1,
		SPELL_2,
		SPELL_3,
		SPELL_4,
		SPELL_5,
		SPELL_6,
		SPELL_7,
		SPELL_8,
		TOTAL
	};
}

#include "Controller.hpp"
