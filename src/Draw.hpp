#pragma once
#include <glm/glm.hpp>

namespace color
{
	constexpr glm::vec3 WHITE  {1.0f};
	constexpr glm::vec3 RED    {1.0f, 0.0f, 0.0f};
	constexpr glm::vec3 GREEN  {0.0f, 1.0f, 0.0f};
	constexpr glm::vec3 BLUE   {0.0f, 0.0f, 1.0f};

	constexpr glm::vec3 YELLOW {1.0f, 1.0f, 0.0f};
	constexpr glm::vec3 CYAN   {0.0f, 1.0f, 1.0f};
	constexpr glm::vec3 MAGENTA{1.0f, 0.0f, 1.0f};

}
static const glm::vec4 no_slice {0.0f,0.0f,1.0f,1.0f};
namespace gert {class Texture;}

void draw_init();
void Draw (const glm::mat4&, const gert::Texture *, glm::vec4 slice=no_slice, glm::vec3 c=color::WHITE);
