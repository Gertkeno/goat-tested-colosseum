#include "SpellCollector.hpp"
#include "AttributeSet.hpp"
#include <fstream>
#include <stdexcept>
#ifndef NDEBUG
#include <bitset>
#endif

#include "DebugOut.hpp"
#include "Dummy.hpp"

inline void trim_space (std::string & s)
{
	while (not s.empty() and std::isspace (s.front()))
		s.erase (s.begin());
	while (not s.empty() and std::isspace (s.back()))
		s.pop_back();
}

inline void command_finder (Spell & s, std::istream & i)
{
	if (s.code != nullptr)
		throw std::runtime_error {"Command block entered twice"};
	std::string command;
	std::vector <bc::key> compiled;
	do
	{
		i >> command;
		if (command.front() == '}')
			break;
		else if (command.front() == '#')
			std::getline (i, command);
		else if (command == "ATTRIBUTE")
		{
			compiled.push_back (bc::PL);
			std::string a;
			i >> a;
			if (a == "SPEED")
				compiled.push_back (SPEED);
			else if (a == "STRENGTH")
				compiled.push_back (STRENGTH);
			else if (a == "MAGIC")
				compiled.push_back (MAGIC);
			else if (a == "DEFENSE")
				compiled.push_back (DEFENSE);
			else if (a == "POISE")
				compiled.push_back (POISE);
			else
				throw std::runtime_error {"Unkown attribute: " + a};

			compiled.push_back (bc::PUSH_ATTRIB);
		}
		else if (command == "LITERAL")
		{
			compiled.push_back (bc::PL);
			float val;
			i >> val;
			compiled.push_back (val);
		}
		else
			compiled.push_back (bc::str_to_code (command.c_str()));
	} while (not i.eof());

	if (compiled.empty())
		throw std::runtime_error {"Command block ended with no commands"};

	compiled.push_back (bc::END);
	s.code = new bc::key [compiled.size()];
	std::copy (compiled.begin(), compiled.end(), s.code);
}

inline void target_finder (Spell & s, std::istream & i)
{
	std::string tword;
	i.get();
	do
	{
		std::getline (i, tword, ',');
		if (tword == "self")
			s.target |= Spell::SELF;
		else if (tword == "ally")
			s.target |= Spell::ALLY;
		else if (tword == "enemy")
			s.target |= Spell::ENEMY;
		else if (tword == "ground")
			s.target |= Spell::GROUND;
		else if (tword == "any")
			s.target = Spell::ANY;
		else
			throw std::runtime_error {"Unknown target \"" + tword + '"'};
	} while (not i.eof() and i.get() != '\n');
}

SpellCollector::SpellCollector (const std::string & filename)
{
	std::ifstream spellList {filename};
	if (not spellList.is_open())
		throw std::runtime_error {"Couldn't open spell list \"" + std::string (filename) + '"'};

	Dummy testTarget, testCaster;
	while (not spellList.eof())
	{
		if (spellList.get() != '{')
			continue;

		Spell temp;
		std::string word;
		do
		{
			try
			{
				spellList >> word;
				// comment and close
				if (word.front() == '}')
					break;
				else if (word.front() == '#')
					std::getline (spellList, word);
				// parse labels
				else if (word == "title:")
				{
					std::getline (spellList, temp.title);
					trim_space (temp.title);
				}
				else if (word == "description:")
				{
					std::getline (spellList, temp.description);
					trim_space (temp.description);
				}
				else if (word == "range:")
					spellList >> temp.range;
				else if (word == "cd:" or word == "cooldown:")
					spellList >> temp.cooldown;
				else if (word == "ap:")
					spellList >> temp.actionPoints;
				else if (word == "target:")
					target_finder (temp, spellList);
				else if (word.back() == '{')
					command_finder (temp, spellList);
				// hard errors are good
				else
					throw std::runtime_error {"Unkown label: \"" + word + '"'};
			} // try
			catch (const std::exception & e)
			{
				const auto preface {"From file \"" + std::string (filename) + "\" "};
				const auto name {"in spell \"" + temp.title + "\": "};
				throw std::runtime_error {preface + name + e.what()};
			}
		} while (not spellList.eof());

		DE_OUT ("Spell Name:\t\"" << temp.title
		<< "\"\nDescription:\t\"" << temp.description
		<< "\"\nRange:   \t" << temp.range
		<< "\nAP:      \t" << temp.actionPoints
		<< "\nCoolDown:\t" << temp.cooldown
		<< "\nTargets: \t" << std::bitset<8> (temp.target) << '\n');
		if (temp.code == nullptr)
		{
			const auto preface {"From file \"" + std::string (filename) + "\" "};
			const auto name {"in spell \"" + temp.title + "\": "};
			throw std::runtime_error {preface + name + "Missing commands"};
		}

		bc::run (temp.code, &testTarget, &testCaster, glm::vec2());
		_collection.push_back (std::move (temp));
	} // while not eof

	if (_collection.empty())
		throw std::runtime_error {"Collection empty from file " + std::string (filename)};
}

SpellCollector::~SpellCollector()
{
	for (auto & i : _collection)
		delete [] i.code;
}

const Spell * SpellCollector::find_name (const std::string & name) const
{
	for (auto & i : _collection)
		if (i.title == name)
			return & i;
	throw std::runtime_error {"Couldn't find spell named " + name};
}
