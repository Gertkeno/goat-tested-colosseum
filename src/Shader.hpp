#ifndef GERT_SHADER_H
#define GERT_SHADER_H

#include <map>
#include <string>
#include <glm/detail/type_vec.hpp>

using GLuint = unsigned int;
struct SDL_RWops;

namespace gert
{
class Shader
{
public:
	Shader (const char * fragfile, const char * vertfile);
	Shader (const void * fdata, unsigned fsize, const void * vdata, unsigned vsize);
	~Shader();

	enum shade_t: unsigned char
	{
		FRAGMENT,
		VERTEX,//keep these in order
		TOTAL
	};

	static Shader* Active_Shader;

	void use_shader (void);

	void set_uniform (const std::string& title, float value);
	void set_uniform (const std::string& title, int value);
	void set_uniform (const std::string& title, GLuint value);
	void set_uniform (const std::string& title, glm::vec3 value);
	void set_uniform (const std::string& title, glm::vec4 value);

	GLuint get_program (void);
private:
	void _init (SDL_RWops *, shade_t);
	int get_location (const std::string& title);

	std::map<std::string, int> _recentUniform;
	GLuint _shaders[TOTAL];
	GLuint _program;
};
}

#endif // GERT_SHADER_H
