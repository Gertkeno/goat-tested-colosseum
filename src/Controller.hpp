#pragma once

#include <vector>
#include <iostream>
#include <SDL2/SDL_events.h>
#include <glm/glm.hpp>

namespace gert
{
struct Input
{
	enum struct type_t: char {KEYBOARD, JOYBUTTON, JOYAXIS, MOUSE, NONE} myType;

	long int button;//SDL_keycode, button, mousebutton or axis id
	short axisMax; //just for axis
	bool axisPass; //passed framePress threshold

	float held;//0.0 to 1.0
	bool framePress;

	constexpr Input (type_t t, long b, short a)
	: myType {t}
	, button {b}
	, axisMax {a}
	, axisPass {false}
	, held {0.0f}
	, framePress {false}
	{}
	constexpr Input(type_t t, long b): Input (t, b, 0) {}
	constexpr Input(): Input (type_t::NONE, 0) {}
};

std::ostream & operator << (std::ostream & out, const Input &);
std::istream & operator >> (std::istream & in, Input &);

using cunit = std::pair <size_t, Input>;
class Controller
{
public:
	Controller (const char * filname);
	Controller (const cunit * data, size_t);

	void manage_inputs (const SDL_Event*);

	float get_input  (size_t name) const {return _data [name].held;}
	float operator[] (size_t name) const {return get_input (name);}
	bool  get_press  (size_t name) const {return _data [name].framePress;}
	bool  operator() (size_t name) const {return get_press (name);}

	bool joy_add (int deviceIndex);
	bool joy_remove (SDL_JoystickID instanceID);
	int joy_count() const {return _joys.size();}

	bool save_file (const char * filename) const;
	bool mutate_value (size_t, const Input&);
	size_t input_count() const {return _data.size();}

	void reset_press();
	void reset_held();

	static void set_window_wh (const float& w, const float& h) {_mouseDivisor = {w, h};}
	static glm::vec2 get_mouse_normal() {return _mouseCalc;}

	void debug_draw (void(*) (int, float));
private:
	std::vector<Input> _data;
	std::vector<SDL_Joystick*> _joys;

	bool _tracked_joystick (SDL_JoystickID);
	template<typename Func>
	void _search_input (Input::type_t t, int id, Func&& x)
	{
		for (auto & i: _data)
		{
			if (i.myType != t or i.button != id)
				continue;

			x (&i);
		}
	}

	static glm::vec2 _mouseDivisor;
	static glm::vec2 _mouseCalc;
};
} // namespace gert
