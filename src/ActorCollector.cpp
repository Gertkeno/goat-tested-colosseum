#include "ActorCollector.hpp"
#include "Modules.hpp"

#include <fstream>
#include <stdexcept>
#include <iostream>

inline void trim_space (std::string & s)
{
	while (not s.empty() and std::isspace (s.front()))
		s.erase (s.begin());
	while (not s.empty() and std::isspace (s.back()))
		s.pop_back();
}

// need to somehow access spell data
inline void spell_assignment (Enemy::spell_c & s, std::istream & i)
{
	std::string spellName;
	std::string spellBase {"base"};
	i.get();
	int sindex {0};
	do
	{
		std::getline (i, spellName, ',');
		if (spellName.front() == '@')
			spellBase = spellName.substr (1);
		else
			s [sindex++] = modules::get_spell (spellBase, spellName);
	} while (not i.eof() and i.get() != '\n');
}

inline attrib_t attrib_from_str (const std::string & s)
{
	// order matters :)
	// AttributeSet.hpp
	const char * names [] = {"poise:", "speed:", "strength:", "magic:", "defense:",};
	for (int i {0}; i < attrib_t::TOTAL; ++i)
	{
		if (s == names [i])
			return static_cast <attrib_t> (i);
	}
	return attrib_t::TOTAL;
}

ActorCollector::ActorCollector (const std::string & filename)
{
	std::ifstream actorList {filename};
	if (not actorList.is_open())
		throw std::runtime_error {"Couldn't open actor list \"" + std::string (filename) + '"'};

	ActorParent tmp;
	while (not actorList.eof())
	{
		if (actorList.get() != '{')
			continue;

		//ActorParent tmp {nullptr};
		std::string word;
		do
		{
			actorList >> word;
			const auto at {attrib_from_str (word)};
			if (word.front() == '}')
				break;
			else if (word.front() == '#')
				std::getline (actorList, word);
			else if (at != attrib_t::TOTAL)
			{
				float attrget;
				actorList >> attrget;
				tmp.as [at] = attrget;
			}
			else if (word == "title:")
			{
				std::getline (actorList, tmp.name);
				trim_space (tmp.name);
			}
			else if (word == "radius:")
				actorList >> tmp.radius;
			else if (word == "texture:")
			{
				std::getline (actorList, tmp.texturefile);
				trim_space (tmp.texturefile);
			}
			else if (word == "spells:")
				spell_assignment (tmp.spell, actorList);
			else
				throw std::runtime_error {"Unkown label \"" + word + '"'};

		} while (not actorList.eof());
	}

	_collection.push_back (std::move (tmp));
}
